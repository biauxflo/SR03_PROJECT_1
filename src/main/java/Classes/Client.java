package Classes;

import java.net.Socket;
import java.util.UUID;

/** Classe définissant les clients.*/
public class Client {

    /** Id du client. */
    private final String id;

    /** Socket de connexion du client. */
    private final Socket socket;

    /** Pseudo du client. */
    private String pseudo;

    /** Constructeur public.
     * @param socket Socket de connexion du client.
     * @param pseudo Pseudo du client connecté.
     */
    public Client(Socket socket,String pseudo) {
        this.id = UUID.randomUUID().toString();
        this.socket = socket;
        this.pseudo = "";
    }

    /** Retourne l'ID du client.
     * @return ID du client.
     */
    public String getId() {
        return id;
    }

    /** Retourne le socket du client.
     * @return Socket du client.
     */
    public Socket getSocket() {
        return socket;
    }

    /** Retourne le pseudo du client.
     * @return Pseudo du client.
     */
    public String getPseudo() {
        return pseudo;
    }

    /** Défini le pseudo du client.
     * @param pseudo Pseudo du client à définir.
     */
    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    /** Override du toString.
     * @return L'id du client mis en forme.
     */
    public String toString(){
        return ("Id du client : "+getId());
    }
}
